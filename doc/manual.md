# Introduction

This manual in written in [Markdown](http://www.writethedocs.org/guide/writing/Markdown_basics/), compiled with [Sphinx](http://www.sphinx-doc.org/en/master/index.html), and hosted on [GitLab Pages](https://about.gitlab.com/features/pages/). 

It focuses on how to write __User Manuals__ for the Reconcell project and is structured as follows. In the [firts section](#just-fork-it) we will look at how to... 



## Just fork it


## The Sphinx that you don't need


## It's all about the Markdown

### Where to write?

#### Offline options

#### Online options

- Gitlab
- [StackEdit](https://stackedit.io/)

### How to write?


# __TEST SECTION__

This is Markdown

## List test

* Item 1
* Item 2
  * Item 2a
* Item 3

## Image test
 
![Reconcell logo](/doc/ReconCellLogo.png)
*Simple caption*

## Code Test

Inline `code` has `back-ticks around` it.

Blocks of code are fanced with three back ticks or are indented with four spaces.
```
for i=1
end
```

    for i=1
	end


## Table test

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

