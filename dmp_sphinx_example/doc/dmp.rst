DMP library
=========================

The DMP encode library was developed at JSI to be used in Python. At the current version, the library only provides the DMP encoding functions.

For code snippets and the documentation how to use this library, please reffer to the following sections.

Python DMP module
=========================

.. automodule:: dmp_sphinx_example

.. automodule:: dmp_sphinx_example.dmp
   :members: