.. Sphinx ReconCell documentation master file, created by
   me on Fri April 6, 2018.

Welcome to the ReconCell manual writing manual!
==================================================

Contents:

.. toctree::
	:maxdepth: 2
	
	doc/manual.md


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
